/* 
	Using the E-commerce Classes
	Demonstrate how to use the classes Customer, Product, and Cart to create an e-commerce system.
*/
/*
Demonstrate how to use the classes Customer, Product, and Cart to create an e-commerce system. We'll go through the process of adding products to a cart, updating quantities, checking out, displaying the order details, as well as other available methods.

Step 1: Create a Customer

  Instantiate a Customer object by providing an email address.
  Example: const customer = new Customer('john@example.com');

Step 2: Add Products to the Cart
  Instantiate Product objects by providing the name and price.

  const product1 = new Product('Soap', 9.99);
  const product2 = new Product('Shampoo', 12.99);

  Add products to the cart using the addToCart method of the customer's cart.

  Specify the product and quantity as arguments.

  customer.cart.addToCart(product1, 3);
  customer.cart.addToCart(product2, 2);

  After adding the products, the cart contents will be as follows:

  console.log(customer.cart.contents);

  // Output: 
    [ 
      { product: Product { name: 'Soap', price: 9.99, isActive: true }, quantity: 3 },{ product: Product { name: 'Shampoo', price: 12.99, isActive: true }, quantity: 2 } 
    ]
  
Step 3: Update Product Quantity in the Cart

  Use the updateProductQuantity method of the cart to modify the quantity of a specific product.

  Provide the product name and the desired quantity as arguments.

  customer.cart.updateProductQuantity('Soap', 5);

  After updating the quantity, the cart contents will be as follows:

  console.log(customer.cart.contents);
  // Output: [ { product: Product { name: 'Soap', price: 9.99, isActive: true }, quantity: 5 },
  //           { product: Product { name: 'Shampoo', price: 12.99, isActive: true }, quantity: 2 } ]

Step 4: Display Cart Contents

  Use the showCartContents method of the cart to display the current contents of the cart.

  customer.cart.showCartContents();

  // Output: [ { product: Product { name: 'Soap', price: 9.99, isActive: true }, quantity: 5 },
  //            { product: Product { name: 'Shampoo', price: 12.99, isActive: true }, quantity: 2 } ]

Step 5: Compute Cart Total

  Use the computeTotal method of the cart to calculate the total amount for all products in the cart.

  customer.cart.computeTotal();

  After computing the total, you can access the totalAmount property of the cart to see the calculated total:

  console.log(customer.cart.totalAmount);
  // Output: 78.93

Step 6: Check Out and Create an Order

  Use the checkOut method of the customer to finalize the purchase and create an order.

  customer.checkOut();

  After checking out, the order details will be stored in the orders property of the customer:


  console.log(customer.orders);
  // Output: [ { products: [ { product: Product { name: 'Soap', price: 9.99, isActive: true }, quantity: 5 },
  //                        { product: Product { name: 'Shampoo', price: 12.99, isActive: true }, quantity: 2 } ],
  //             totalAmount: 75.93 } ]

Step 7: Archive a Product

  Use the archive method of a Product object to mark it as inactive or archived.

  product1.archive();

  After archiving a product, the isActive property of that product will be set to false.

Step 8: Update Product Price

  Use the updatePrice method of a Product object to change its price.

  Provide the new price as an argument.

  product2.updatePrice(14.99);

  After updating the price, the price property of the product will be modified accordingly.
*/

class Customer {
    constructor(email) {
        this.email = email;
        this.cart = new Cart();
        this.orders = [];
    }

    checkOut() {
        this.orders.push(this.cart.contents, this.cart.totalAmount);
    	this.cart.clearCartContents();
        return this;
    }
}

class Product {
    constructor(name, price) {
        this.name = name;
        this.price = price;
        this.isActive = true;
    }

    archive() {
        this.isActive = false;
        return this;
    }

    updatePrice(newPrice) {
        this.price = newPrice;
        return this;
    }
}

class Cart {
    constructor() {
        this.contents = [];
        this.totalAmount = 0;
    }

    addToCart(product, quantity) {
        this.quantity = quantity;
    	this.contents.push({product, quantity});
    	this.totalAmount = this.computeTotal()
    	return this;
    }

    showCartContents() {
        return this.contents
    }

    updateProductQuantity(name, quantity) {
        let changeQuantity = this.contents.filter(result => {
        	return (result.product.name === name)
        })

        changeQuantity[0].quantity = quantity;
        return this.contents;
    }

    clearCartContents() {
        this.contents = [];
        this.quantity = 0;
        this.totalAmount = 0;
        return this;
    }

    computeTotal() {
        let totalArr = this.contents.map(result => {
        	return (result.product.price * result.quantity);
        })
        console.log(totalArr);
        return this.totalAmount = totalArr.reduce((a,b)=>a+b);
    }
}

const customer = new Customer('john@example.com');

const product1 = new Product('Soap', 9.99);
const product2 = new Product('Shampoo', 12.99);

customer.cart.addToCart(product1, 3);
customer.cart.addToCart(product2, 2);

console.log(product1);

console.log(customer.cart.contents);

customer.cart.showCartContents();