// Use an object literal: {} to create an object representing a user.
    // Encapsulation
    // Whenever we add properties or methods to an object, we are already performing ENCAPSULATION
    // The organization of information (as properties) and behavior (as methods) to belong to the object that encapsulates them
    // (the scope of encapsulation is denoted by object literals)

let studentOne = {
    name: "John",
    email: "john@mail.com",
    grades: [89, 84, 78, 88],

    // methods
        // add the functionalities available to a student as object methods
        // the keyword "this" refers to the object encapsulating the method where "this" is called
    login(){
        console.log(`${this.email} has logged in`);
    },

    logout(){
        console.log(`${this.email} has logged out`);
    },

    listGrades(){
        console.log(`${this.name}'s quarterly grade averages are ${this.grades}`);
    },

    // Mini Activity
        // Create a function that will get the quarterly average of studentOne's grades
    average(){
        let avg = 0;
        for(let i = 0; i < this.grades.length; i++){
            avg = avg + this.grades[i];
        }
        return avg = avg/this.grades.length;
    },

    // Mini Activity 2
        // Create a function that will return true if average grade is >= 85, false otherwise
    isPassed(){
        let avg = 0;
        for(let i = 0; i < this.grades.length; i++){
            avg = avg + this.grades[i];
        }
        avg = avg/this.grades.length;
        if(avg >= 85){
            return true;
        } else {
            return false;
        }
    },


    // Mini Activity 3
        // Create a function called willPassWithHonors() that returns true if the student has passed and their average is >= 90. The function returns false if either one is not met.
    willPassWithHonors(){
        let avg = 0;
        for(let i = 0; i < this.grades.length; i++){
            avg = avg + this.grades[i];
        }
        avg = avg/this.grades.length;
        if(avg >= 90){
            return true;
        } else {
            return false;
        }
    }


}

// log the content of studentOne's encapsulated information in the console
console.log(`student one's name is ${studentOne.name}`);
console.log(`student one's email is ${studentOne.email}`);
console.log(`student one's quarterly grades is ${studentOne.grades}`);
console.log(studentOne);

