//1. Translate the other students from our boilerplate code into their own respective objects.

//2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

//3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.

//4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

//5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.

//6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

//7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

//8. Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.

//9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

let studentOne = {
    name: "John",
    email: "john@mail.com",
    grades: [89, 84, 78, 88],
    average(){
        let avg = 0;
        for(let i = 0; i < this.grades.length; i++){
            avg = avg + this.grades[i];
        }
        return avg = avg/4;
    },
    willPass(){
        if(this.average() >= 85){
            return true;
        } else {
            return false;
        }
    },
    willPassWithHonors(){
        if(this.average() >= 90){
            return true;
        } else if(this.average() >= 85 && this.average() < 90){
            return false;
        } else {
        	return undefined;
        }
    }
}

let studentTwo = {
    name: "Joe",
    email: "joe@mail.com",
    grades: [78, 82, 79, 85],
    average(){
        let avg = 0;
        for(let i = 0; i < this.grades.length; i++){
            avg = avg + this.grades[i];
        }
        return avg = avg/4;
    },
    willPass(){
        if(this.average() >= 85){
            return true;
        } else {
            return false;
        }
    },
    willPassWithHonors(){
        if(this.average() >= 90){
            return true;
        } else if(this.average() >= 85 && this.average() < 90){
            return false;
        } else {
        	return undefined;
        }
    }
}

let studentThree = {
    name: "Jane",
    email: "jane@mail.com",
    grades: [87, 89, 91, 93],
    average(){
        let avg = 0;
        for(let i = 0; i < this.grades.length; i++){
            avg = avg + this.grades[i];
        }
        return avg = avg/4;
    },
    willPass(){
        if(this.average() >= 85){
            return true;
        } else {
            return false;
        }
    },
    willPassWithHonors(){
        if(this.average() >= 90){
            return true;
        } else if(this.average() >= 85 && this.average() < 90){
            return false;
        } else {
        	return undefined;
        }
    }
}

let studentFour = {
    name: "Jessie",
    email: "jessie@mail.com",
    grades: [91, 89, 92, 93],
    average(){
        let avg = 0;
        for(let i = 0; i < this.grades.length; i++){
            avg = avg + this.grades[i];
        }
        return avg = avg/4;
    },
    willPass(){
        if(this.average() >= 85){
            return true;
        } else {
            return false;
        }
    },
    willPassWithHonors(){
        if(this.average() >= 90){
            return true;
        } else if(this.average() >= 85 && this.average() < 90){
            return false;
        } else {
        	return undefined;
        }
    }
}

let classOf1A = {
	students: [studentOne, studentTwo, studentThree, studentFour],
	countHonorStudents(){
		let counter=0;
		this.students.forEach(each => {
			if(each.average() >= 90){
				counter++;
			}
		})
		return counter;
	},
	honorsPercentage(){
		return this.countHonorStudents()/this.students.length * 100
	},
	retrieveHonorStudentInfo(){
		let arrHonor = []
		this.students.forEach(honor => {
			if(honor.average() >= 90){
				arrHonor.push({
					email : honor.email,
					aveGrade : honor.average()
				})
			}
		})
		return arrHonor;
	},
	sortHonorStudentsByGradeDesc(){
		this.retrieveHonorStudentInfo().sort()
		return this.retrieveHonorStudentInfo().reverse()
	}
}